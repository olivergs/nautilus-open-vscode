import os

try:
    from urllib import unquote
except ImportError:
    from urllib.parse import unquote

import gi
gi.require_version('GConf', '2.0')
from gi.repository import Nautilus, GObject, GConf


class OpenVSCodeExtension(Nautilus.MenuProvider, GObject.GObject):
    def __init__(self):
        self.client = GConf.Client.get_default()
        
    def _open_vscode(self, file):
        filename = unquote(file.get_uri()[7:])

        os.chdir(filename)
        os.system('code .')
        
    def callback(self, menu, file):
        self._open_vscode(file)
       
    def get_file_items(self, window, files):
        if len(files) != 1:
            return
        
        file = files[0]
        if not file.is_directory() or file.get_uri_scheme() != 'file':
            return
       
        item = Nautilus.MenuItem(name='NautilusPython::openvscode_file_item',
                                 label='Open Visual Studio Code' ,
                                 tip='Open Visual Studio Code In %s' % file.get_name())
        item.connect('activate', self.callback, file)
        return item,

    def get_background_items(self, window, file):
        item = Nautilus.MenuItem(name='NautilusPython::openvscode_file_item2',
                                 label='Open Visual Studio Code' ,
                                 tip='Open Visual Studio Code In %s' % file.get_name())
        item.connect('activate', self.callback, file)
        return item,
